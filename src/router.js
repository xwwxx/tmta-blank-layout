import { Vue } from "tmta-core";
import Router from "vue-router";
import Layout from "@/Layout.vue";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/demo",
      name: "demo",
      component: Layout,
      redirect: "/demo/page-1",
      children: [
        {
          path: "page-1",
          name: "demo-page-1",
          component: {
            template:
              '<div style="background-color: #67C23A; padding: 20px;">Hi Daniel</div>'
          }
        },
        {
          path: "page-2",
          name: "demo-page-2",
          component: {
            template:
              '<div style="background-color: #409EFF; padding: 20px;">Hello World</div>'
          }
        }
      ]
    }
  ]
});
