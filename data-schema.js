export default {
  type: "object",
  shortDesc: {
    en:
      'TODO: Write a brief description here. it is more better to add some pictures.</br/><image src="https://unpkg.com/tmta-demo-component/dist/assets/thumbnail.png"/>',
    zh_cn:
      'TODO：写下简单描述，如果有配图那更好</br/><image src="https://unpkg.com/tmta-demo-component/dist/assets/thumbnail.png"/>'
  },
  properties: {
    style: {
      type: "object",
      description: {
        en: "Style",
        zh_cn: "样式"
      },
      properties: {
        padding: {
          type: "string",
          description: {
            en: "Padding",
            zh_cn: "边距"
          },
          enum: [
            {
              value: "10px 10px",
              desc: {
                en: "Just a demo value",
                zh_cn: "仅仅是演示的数据"
              }
            }
          ]
        }
      }
    }
  },
  default: {
    style: {
      padding: "10px"
    }
  }
};
