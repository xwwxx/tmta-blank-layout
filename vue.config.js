const path = require("path");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const merge = require("webpack-merge");

function resolve(dir) {
  return path.join(__dirname, dir);
}

module.exports = merge(
  {
    lintOnSave: false,
    css: {
      extract: false
    },
    configureWebpack: {
      resolve: {
        alias: {
          vue$: "vue/dist/vue.esm.js"
        }
      },
      plugins: [
        new CopyWebpackPlugin([
          {
            from: resolve("src/assets"),
            to: resolve("dist/assets"),
            toType: "dir"
          }
        ])
      ]
    }
  },
  process.env.NODE_ENV === "production"
    ? {
        configureWebpack: {
          externals: {
            "tmta-core": {
              root: "tmtaCore",
              commonjs2: "tmta-core",
              commonjs: "tmta-core",
              amd: "tmtaCore"
            }
          }
        }
      }
    : {}
);
