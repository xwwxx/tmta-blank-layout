import { expect } from "chai";
import { shallowMount } from "@vue/test-utils";
import BlankLayout from "@/components/BlankLayout.vue";

describe("BlankLayout.vue", () => {
  it("renders props.config when passed", () => {
    const config = {
      style: {
        padding: "20px"
      }
    };
    const wrapper = shallowMount(BlankLayout, {
      propsData: { config }
    });
    expect(wrapper.element.querySelector("div").style.padding).to.equal(
      config.style.padding
    );
  });
});
