// https://docs.cypress.io/api/introduction/api.html

describe("Blank Layout Test", () => {
  it("Visits the app root url", () => {
    cy.visit("/");
    cy.contains("Route-1").click();
    cy.get(".blank-layout").should("contain", "Hi Daniel");

    cy.contains("Route-2").click();
    cy.get(".blank-layout").should("contain", "Hello World");
  });
});
