# tmta-blank-layout

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run your end-to-end tests
```
npm run test:e2e
```

### Run your unit tests
```
npm run test:unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Note

- 约定：布局的缩略图放在`src/assets`目录，并且以`thumbnail.png`命名 

### Release

1. 构建组件压缩包

```
$ tmta zip
```

2. 将生成的组件压缩包上传到平台
